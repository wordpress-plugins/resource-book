<?php 
abstract class MainResource{  
    protected $rsClass         =''; 
    protected $rsFormat        ='';
    protected $rowName         ='';
    protected $rowDescription  ='';
    protected $rowAuthor       ='';
    protected $rowCover        ='';
    protected $rowTimeVideo    ='0:0:0';
    protected $rowFile         ='';
    protected $rowNameVideo    ='';
    protected $rowLinkSite     ='';

    function __construct ($rsData) {
        if(isset($rsData['rs_format'])) {
            $this->rsFormat  = $rsData['rs_format'];
            if ($rsData['rs_format'] == $this->rsClass){
                $this->rowName          = (isset($rsData['rs_name'])        ? $rsData['rs_name']        : '');
                $this->rowDescription   = (isset($rsData['rs_description']) ? $rsData['rs_description'] : '');
                $this->rowAuthor        = (isset($rsData['rs_author'])      ? $rsData['rs_author']      : '');
                $this->rowCover         = (isset($rsData['rs_cover'])       ? $rsData['rs_cover']       : '');
                $this->rowTimeVideo     = (isset($rsData['rs_time_video'])  ? $rsData['rs_time_video']  : '0:0:0');
                $this->rowFile          = (isset($rsData['rs_file_doc'])    ? $rsData['rs_file_doc']    : '');
                $this->rowNameVideo     = (isset($rsData['rs_link_video'])  ? $rsData['rs_link_video']  : '');
                $this->rowLinkSite      = (isset($rsData['rs_link_site'])   ? $rsData['rs_link_site']   : '');
            }
        }   
        $this->construct_description();
        $this->construct_author();
        $this->construct_cover();
    }

    protected function construct_name(){
        $Class = $this->rsClass;
        $Name = $this->rowName;

        $labelText = 'Название';
        switch ($Class) {
            case 'rs-book':     $labelText .= ' книги';  break;
            case 'rs-video':    $labelText .= ' видео';  break;
        }

        $checkContent = ($Name == '' ? '' : 'rs-input-successfully' );

        $code  ='<td width="200px"><label for="'.$Class.'-name">'.$labelText.'</label></td>';
        $code .='<td width="400px"><input type="text" class="rs-name" name="'.$Class.'-name" id="'.$Class.'-name" value="'.$Name.'"></td>';
        $code .='<td><span class="rs-cheсk-control '.$checkContent.'" id="'.$Class.'-check-name"></span></td>';
        $code = '<tr>'.$code.'</tr>';

        $this->rowName = $code;
    }

    protected function construct_description(){
        $Class = $this->rsClass;
        $Description = $this->rowDescription;

        $labelText = 'Описание';
        switch ($Class) {
            case 'rs-book':     $labelText .= ' книги';  break;
            case 'rs-video':    $labelText .= ' видео';  break;
            case 'rs-link':     $labelText .= ' статьи'; break;
        }
        $checkContent = ($Description == '' ? '' : 'rs-input-successfully' );

        $code = '<td width="200px"><label for="'.$Class.'-description">'.$labelText.'</label></td>';
        $code .= '<td width="400px"><textarea name="'.$Class.'-description" class="rs-description" id="'.$Class.'-description" cols="41" rows="4">'.$Description.'</textarea></td>';
        $code .= '<td><span class="rs-cheсk-control '.$checkContent.'" id="'.$Class.'-check-description"></span></td>';
        $code = '<tr>'.$code.'</tr>';

        $this->rowDescription = $code;
    }

    protected function construct_author(){
        $Class = $this->rsClass;
        $Author = $this->rowAuthor;

        $labelText = 'Автор';
        switch ($Class) {
            case 'rs-book':     $labelText .= ' книги';  break;
            case 'rs-video':    $labelText .= ' видео';  break;
            case 'rs-link':     $labelText .= ' статьи'; break;
        }
        $checkContent = ($Author == '' ? '' : 'rs-input-successfully' );

        $code  = '<td width="200px"><label for="'.$Class.'-author">'.$labelText.'</label></td>';
        $code .= '<td width="400px"><input type="text" class="rs-author" name="'.$Class.'-author" id="'.$Class.'-author" value="'.$Author.'"/></td>';
        $code .= '<td><span class="rs-cheсk-control '.$checkContent.'" id="'.$Class.'-check-author"></span></td>';
        $code = '<tr>'.$code.'</tr>';

        $this->rowAuthor = $code;
    }

    protected function construct_cover(){
        $Class = $this->rsClass;
        $Cover = $this->rowCover;

        $labelText = 'Обложка';
        switch ($Class) {
            case 'rs-book':     $labelText .= ' книги';  break;
            case 'rs-video':    $labelText .= ' видео';  break;
            case 'rs-link':     $labelText .= ' статьи'; break;
        }
        $checkContent = ($Cover == '' ? '' : 'rs-input-successfully' );
        $labelText .= ($checkContent == '' ? '': ' уже есть, но вы можете выбрать другую');

        $code = '<td width="200px"><label for="'.$Class.'-cover">'.$labelText.'</label>'.($Cover == '' ? '':'<input name="rs-cover" type="text" value="'.$Cover.'" hidden>').'</td>';
        $code .= '<td width="400px"><input type="file" name="'.$Class.'-cover" id="'.$Class.'-cover" class="" accept=".jpg,.jpeg,.png"></td>';
        $code .= '<td><span class="rs-cheсk-control '.$checkContent.'" id="'.$Class.'-check-cover"></span></td>'; 
        $code = '<tr>'.$code.'</tr>';

        $this->rowCover = $code;
    }

    protected function construct_time(){
        $Class = $this->rsClass;
        $Time  = $this->rowTimeVideo;
        list($hour, $minutes, $second) = explode(":", $Time);
        $checkContent = ($Time == '0:0:0' ? '' : 'rs-input-successfully' );
        
        
        $code = '<td width="200px"><label for="'.$Class.'-minutes">Длительность видео</label></td>';
        $code .= '<td width="400px">';
        $code .= '<input type="number" step="1" id="'.$Class.'-hours"   name="'.$Class.'-hours"   min="0" max="9"  value='.$hour.'> :';
        $code .= '<input type="number" step="1" id="'.$Class.'-minutes" name="'.$Class.'-minutes" min="0" max="59" value='.$minutes.'> :';
        $code .= '<input type="number" step="1" id="'.$Class.'-seconds" name="'.$Class.'-seconds" min="0" max="59" value='.$second.'></td>';
        $code .= '<td><span class="rs-cheсk-control '.$checkContent.'" id="'.$Class.'-check-minutes"></span></td>';
        $code = '<tr>'.$code.'</tr>';

        $this->rowTimeVideo = $code;
    }

    protected function construct_file(){
        $Class = $this->rsClass;
        $File  = $this->rowFile;

        $checkContent = ($File == '' ? '' : 'rs-input-successfully' );
        
        $labelText = 'Файл книги';
        $labelText .= ($checkContent == '' ? '': ' уже есть, но вы можете выбрать другой');

        $code  = '<td width="200px"><label for="'.$Class.'-file">'.$labelText.'</label></td>';
        $code .= '<td width="400px"><input type="file" name="'.$Class.'-file" id="'.$Class.'-file" class="" accept=".pdf,.doc,.docx,.ppt,.pptx"></td>';
        $code .= '<td><span class="rs-cheсk-control '.$checkContent.'" id="rs-book-check-file"></span></td>';
        $code = '<tr>'.$code.'</tr>';

        $this->rowFile = $code;
    }

    protected function construct_videoName(){
        $Class = $this->rsClass;
        $NameVideo  = $this->rowNameVideo;

        $checkContent = ($NameVideo == '' ? '' : 'rs-input-successfully' );
        $labelText = 'Ссылка на видео';

        $code  = '<td width="200px"><label for="'.$Class.'-link">'.$labelText.'</label></td>';
        $code .= '<td width="400px">https://youtu.be/<input type="text" class="rs-video-link" name="'.$Class.'-link" id="'.$Class.'-link" value="'.$NameVideo.'"></td>';
        $code .= '<td><span class="rs-cheсk-control '.$checkContent.'" id="rs-video-check-link"></span></td>';
        $code = '<tr>'.$code.'</tr>';

        $this->rowNameVideo = $code;
    }

    protected function construct_siteLink(){
        $Class = $this->rsClass;
        $Link  = $this->rowLinkSite;

        $checkContent = ($Link == '' ? '' : 'rs-input-successfully' );
        $labelText = 'Ссылка на статью';

        $code = '<td width="200px"><label for="'.$Class.'-site">'.$labelText.'</label></td>';
        $code .= '<td width="400px"><textarea name="'.$Class.'-site" id="'.$Class.'-site" cols="41" rows="2">'.$Link.'</textarea></td>';
        $code .= '<td><span class="rs-cheсk-control '.$checkContent.'" id="'.$Class.'-check-site"></span></td>';
        $code = '<tr>'.$code.'</tr>';

        $this->rowLinkSite = $code;
    }
}

class TableBook extends MainResource{
    function __construct($rsData = ''){
        $this->rsClass  = 'rs-book';

        parent::__construct($rsData); 

        $this->construct_name();
        $this->construct_file();
    }

    function getTable(){
        $Class = $this->rsClass;
        $checkContent = (($Class == $this->rsFormat)||($this->rsFormat == '') ? '' : 'rs-table-hidden' );

        $code = '<table cellpadding="10" id="'.$Class.'" class="rs-table '.$checkContent.'">';
        $code .=    $this->rowName;
        $code .=    $this->rowDescription;
        $code .=    $this->rowAuthor;
        $code .=    $this->rowCover;
        $code .=    $this->rowFile;
        $code .= '</table>';

        return $code;
    }
}

class TableVideo extends MainResource{
    function __construct($rsData = ''){
        $this->rsClass  = 'rs-video';

        parent::__construct($rsData); 

        $this->construct_name();
        $this->construct_videoName();
        $this->construct_time();
    }

    function getTable(){
        $Class = $this->rsClass;
        $checkContent = ($Class == $this->rsFormat ? '' : 'rs-table-hidden' );

        $code = '<table cellpadding="10" id="'.$Class.'" class="rs-table '.$checkContent.'">';
        $code .=    $this->rowName;
        $code .=    $this->rowDescription;
        $code .=    $this->rowNameVideo;
        $code .=    $this->rowAuthor;
        $code .=    $this->rowTimeVideo;
        $code .=    $this->rowCover;
        $code .= '</table>';

        return $code;
    }
}

class TableLink extends MainResource{
    function __construct($rsData = ''){
        $this->rsClass  = 'rs-link';

        parent::__construct($rsData); 
        $this->construct_siteLink();
    }

    function getTable(){
        $Class = $this->rsClass;
        $checkContent = ($Class == $this->rsFormat ? '' : 'rs-table-hidden' );

        $code = '<table cellpadding="10" id="'.$Class.'" class="rs-table '.$checkContent.'">';
        $code .=    $this->rowDescription;
        $code .=    $this->rowAuthor;
        $code .=    $this->rowLinkSite;
        $code .=    $this->rowCover;
        $code .= '</table>';

        return $code;
    }
}

class WrapTable {
    protected static $Format;
    protected static $AllSection;
    protected static $UseSection;
    protected static $Tables;

    function __construct($rsFormat, $allSection, $sectionUsed, $tables){
        self::$Format       = $rsFormat;
        self::$AllSection   = $allSection;
        self::$UseSection   = $sectionUsed;
        self::$Tables       = $tables;

        $this->wrap();
    }
    protected function wrap(){
        $code  = $this->rs_header();
        $code .= self::$Tables;
        $code .= $this->rs_button();

        $code = '<form action="'.esc_url(remove_query_arg('msg')).'" method="POST" enctype="multipart/form-data">'.$code.'</form>';
        
        echo $code;
    }

    protected function rs_header(){
        $code = '<table cellpadding="10" style="border-bottom:1px solid;">';

       
        $code .= '<tr>'.$this->rs_format();
        $code .='<td rowspan="4">Использовать в разделе:</td>';
    
        $firstIndex = true;
        if(self::$AllSection){
            foreach (self::$AllSection as $value) {
                if ($firstIndex) {
                    $firstIndex = false;
                } else {
                    $code .= '<tr>';
                }
                $sectionCode = $value->shortcode_section;
                $code .='<td><label for="rs-section-'.$sectionCode.'">'.$value->name_section.'</label></td>';
                $sectionID = $value->id;
                $selected ='';
                if (self::$UseSection){
                    foreach (self::$UseSection as $value) {
                        if ($value->id_section == $sectionID) {
                            $selected ='checked';
                        }
                    }    
                }
                $code .='<td><input type="checkbox" name="rs-section-'.$sectionCode.'" id="rs-section-'.$sectionCode.'" '.$selected.'></td></tr>';
            }
        } else {
            $code .= '<td>Нет доступных разделов, добавьте их в настройках</td></tr>';
        }
        $code .='</table>';
    
        return $code;
    
    }

    protected function rs_format(){
        $code ='<td rowspan="4"><label for="rs-format">Выберите формат: ';
        $code .='<select name="rs-format" id="rs-format">';
            $code .='<option value="rs-book"  '.(self::$Format =='rs-book'  ?'selected' : '').'>Файл</option>';
            $code .='<option value="rs-video" '.(self::$Format =='rs-video' ?'selected' : '').'>Видео</option>';
            $code .='<option value="rs-link"  '.(self::$Format =='rs-link'  ?'selected' : '').'>Ссылка</option>';
        $code .='</select></label></td>';

        return $code;
    }

    protected function rs_button(){
        $value = ( self::$Format == '' ? 'Сохранить' : 'Обновить');

        $code = '<table><tr><td>';
        $code .= '<input type="submit" name="rs-button" id="rs-button" value="'.$value.'">'; 
        $code .= '</td><td><span class="rs-message" id="rs-message"></span></td></tr></table>';
    
        return $code;
    }
    
}
?>