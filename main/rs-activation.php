<?php

function create_table(){
    
    global $wpdb;
    require_once ABSPATH.'wp-admin/includes/upgrade.php' ;

    $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset} COLLATE {$wpdb->collate}";
       

    $table_name = "rs_book_section"; 
    $sql = "CREATE TABLE {$table_name} (
            id                  TINYINT     NOT NULL    AUTO_INCREMENT,
            name_section        TINYTEXT    NOT NULL,
            shortcode_section   TINYTEXT    NOT NULL,
            PRIMARY KEY  (id)
        ) {$charset_collate};";
    dbDelta( $sql );
    //default section
    $wpdb->insert( 
        $table_name, 
        array( 
            'id' => 0, 
            'name_section' => 'default', 
            'shortcode_section' => '', 
        )
    ); 

    $table_name = "rs_book_resource"; 
    $sql = "CREATE TABLE {$table_name} (
            id              MEDIUMINT                       NOT NULL AUTO_INCREMENT,
            rs_format       TINYTEXT DEFAULT ''             NOT NULL,
            rs_name         TINYTEXT DEFAULT ''             NOT NULL,
            rs_description  TEXT     DEFAULT ''             NOT NULL,
            rs_author       TINYTEXT DEFAULT ''             NOT NULL,
            rs_cover        TINYTEXT DEFAULT ''             NOT NULL,
            rs_time_video   CHAR(8)  DEFAULT '0:00:00'      NOT NULL,
            rs_file_doc     TINYTEXT DEFAULT ''             NOT NULL,
            rs_link_video   TINYTEXT DEFAULT ''             NOT NULL,
            rs_link_site    TEXT     DEFAULT ''             NOT NULL,
            rs_date         DATE     DEFAULT '0000-00-00'   NOT NULL,
            PRIMARY KEY  (id)
        ) {$charset_collate};";
    dbDelta( $sql );

    $table_name = "rs_published_resources"; 
    $sql = "CREATE TABLE {$table_name} (
            id              TINYINT     NOT NULL    AUTO_INCREMENT,
            id_resource     TINYTEXT    NOT NULL,
            id_section      TINYTEXT    NOT NULL,
            PRIMARY KEY  (id)
        ) {$charset_collate};";
    dbDelta( $sql );
};

function create_dir(){
    $mainDir = '/var/www/html/wp-content/uploads/rs_uploads/';
    if (!file_exists($mainDir)) {
        mkdir($mainDir, 0777, true);
    };
    $dirDocuments = $mainDir.'documents/';
    if (!file_exists($dirDocuments)) {
        mkdir($dirDocuments, 0777, true);
    };
    $dirCover = $mainDir.'cover/';
    if (!file_exists($dirCover)) {
        mkdir($dirCover, 0777, true);
    };
};

create_table();
create_dir();
?>