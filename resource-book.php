<?php 
    /**
    *   Plugin Name: Resource Book
    *   Description: Плагин сборник ресурсов Книг/статей/видео.
    *
    *   Author: Vladislav Oleinikov
    *   Version: 1.0.0
    */

    $data = get_file_data( __FILE__, array('Version'=>'Version'));

    define('RESBOOK_VERSION'    , $data['Version']);
    define('RESBOOK_PLUGIN'     , __FILE__ );
    define('RESBOOK_PLUGIN_DIR' , plugin_dir_path(RESBOOK_PLUGIN) );
    define('RESBOOK_URL',  plugin_dir_url(RESBOOK_PLUGIN) );


    register_activation_hook(__FILE__, 'rs_activate' );
    function rs_activate(){
        require_once 'main/rs-activation.php';
    }

    register_deactivation_hook( __FILE__, 'rs_deactivate' );
    function rs_deactivate(){
        /*
        $dir = '/var/www/html/wp-content/uploads/rs_uploads/documents/03-2020/';
        $files = array_diff(scandir($dir), ['.','..']);
        foreach ($files as $file) {
            (is_dir($dir.'/'.$file)) ? delDir($dir.'/'.$file) : unlink($dir.'/'.$file);
        }
        rmdir($dir);*/
    }

    //тестируем class
    class Resource{
        // объявление свойства
        var $book = 'Переменная из класса';
        
        // объявление метода
        public function displayVar() {
            return $this->book;
        }
    }


    add_action( 'admin_menu', 'register_my_page' );
    add_filter( 'option_page_capability_'.'my_page_slug', 'my_page_capability' );
    function register_my_page(){
        add_menu_page( 'Книги/статьи/видео', 'Книги/статьи/видео', 'edit_others_posts', 'resbook-main', 'menu_main', plugins_url( 'resource-book/admin/css/images/icon.png' ), 6 ); 
        wp_enqueue_style( 'resourse-book', RESBOOK_URL . 'admin/css/admin.css', array(), RESBOOK_VERSION );
    }
    function menu_main(){
        // здесь получают список всех доступных разделов
        // здесь получают все возможные ресурсы
         
        require_once RESBOOK_PLUGIN_DIR.'admin/admin_page.php';
    }
    function my_page_capability( $capability ) {
	    return 'edit_others_posts';
    }


    add_action( 'plugins_loaded', 'rs_init' );
    function rs_init(){
        add_shortcode( 'resource-book', 'rs_shortcode_init' );
    }
    function rs_shortcode_init( $atts ){
        $atts = shortcode_atts( array(
            'section'  => 'all', // вывод всех
        ), $atts, 'resource-book' );

        //здесь получают список всех доступных разделов
        //здесь получают ресурсы опубликованные в конкретном разделе
        require_once 'users/user_page.php';
        return $html;
    }
?>