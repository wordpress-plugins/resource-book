<?php 
    /** нужно заменить на класс */
    //получить все доступные ресурсы
    if(isset($_GET['delet_res'])) {
        echo delet_resource($_GET['delet_res']);
    }
    function delet_resource($res_id){
        global $wpdb;

        if( ! $res_id = intval($res_id) ) return;
        
        $wpdb->delete( 'rs_published_resources', array( 'id_resource' => $res_id ));
        $wpdb->delete( 'rs_book_resource', array( 'id' => $res_id ));

        //добавить проверку на повтор файлов, если их нет, то файлы тоже удалить!!!!!!!

        $spanSuccess = '<span class="rs-cheсk-control rs-input-successfully"></span>';
        $out = '<table style="border-bottom:2px solid green;">';
        $out .= '<tr><td>'.$spanSuccess.'</td><td>Ресурс удален!</td></tr></table>';
        return $out;
    }
    global $wpdb;
    $resource = $wpdb->get_results( "SELECT id, rs_format, rs_name, rs_description, rs_date FROM rs_book_resource" );


    // получить все доступные разделы 
    //$section = $wpdb->get_results("SELECT * FROM rs_book_section");
    
    $htmlCode = '';
    foreach ($resource as $value) {
        $idResource = $value->id;

        //в каких разделах используеться
        $sectionUsed =  $wpdb->get_results("SELECT * FROM rs_published_resources WHERE id_resource = {$idResource}");
        
        $htmlCode .= contruct_table_row($value, $sectionUsed);
    }
    echo wrapper_table($htmlCode);


    function wrapper_table($htmlTr){

        $htmlTable =    '<table cellpadding="10" border="1" class="rs-list-table">';
        $htmlTable .=       '<thead  style="border-bottom: 1px solid;"><tr> <td colspan=2></td><td>Имя ресурса</td><td>Формат</td><td width="220px">Используеться в разделах</td><td width="130px">Дата создания/обновления</td></tr></thead>';
        $htmlTable .=       '<tbody>'.$htmlTr.'</tbody>';
        $htmlTable .=       '<tfoot><tr><td colspan=2></td><td>Имя ресурса</td><td>Формат</td><td>Используеться в разделах</td><td>Дата создания/обновления</td></tr></tfoot>';
        $htmlTable .=   '</table>';
    
        return $htmlTable;
    }

    function contruct_table_row($resource, $sectionUsed){
        global $wpdb;
        
        $htmlUl = '';
        if ($sectionUsed){
            foreach ($sectionUsed as $value) {
                $idSection = $value->id_section;
                $sectionName = $wpdb->get_var("SELECT name_section FROM rs_book_section WHERE id={$idSection}");

                $htmlUl .= '<li>'.$sectionName.'</li>';
            }
        } else {
            $htmlUl = '<li>Нет используемых разделов</li>';
        }
        $htmlUl = '<ol>'.$htmlUl.'</ol>';
        switch ($resource->rs_format) {
            case 'rs-book':  $format = 'Книга';  break;
            case 'rs-video': $format = 'Видео';  break;
            case 'rs-link':  $format = 'Статья'; break;
        }

        $htmlTr =   '<tr><td><a href="'.add_query_arg( array('delet_res'=>$resource->id )).'"><span class="rs-table-icon rs-icon-bin" title="Удалить элемент: id='.$resource->id.'"></a></td>';
        $htmlTr .=      '<td><a href="'.add_query_arg( array('subpage'=>'add_new','edit_res_id'=>$resource->id )).'"><span class="rs-table-icon rs-icon-pencil" title="Изменить элемент: id='.$resource->id.'"></a></td>';
        $htmlTr .=      '<td>'.($resource->rs_format == "rs-link" ? $resource->rs_description : $resource->rs_name ).'</td>';
        $htmlTr .=       '<td>'.$format.'</td>';
        $htmlTr .=      '<td>'.$htmlUl.'</td>';
        $htmlTr .=      '<td>'.$resource->rs_date.'</td></tr>';

        return $htmlTr;
    }
?>