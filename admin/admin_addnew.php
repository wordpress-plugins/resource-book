<?php
require_once RESBOOK_PLUGIN_DIR.'main/Class-Table-AddNew.php';

    if ($resource_id){
        global $wpdb;
        //получаем ресурс
        $resource = $wpdb->get_results("SELECT * FROM rs_book_resource WHERE id = {$resource_id}");
        $resource = $resource[0];
        //получаем раздел в которых используеться
        $sectionUsed =  $wpdb->get_results("SELECT * FROM rs_published_resources WHERE id_resource = {$resource_id}");
        $rsData = array( 
            'rs_format' => $resource->rs_format,     
            'rs_name'  => $resource->rs_name,       
            'rs_description' => $resource->rs_description,  
            'rs_author' => $resource->rs_author,      
            'rs_cover' => $resource->rs_cover,       
            'rs_time_video' => $resource->rs_time_video,   
            'rs_file_doc' => $resource->rs_file_doc,     
            'rs_link_video' => $resource->rs_link_video,  
            'rs_link_site' => $resource->rs_link_site,    
        );
    } 
    
$book   = new TableBook($rsData);
$video  = new TableVideo($rsData);
$link   = new TableLink($rsData);
$tables = $book ->getTable().$video->getTable().$link ->getTable();

new WrapTable($rsData['rs_format'], $allSection, $sectionUsed, $tables);
   
wp_enqueue_script( 'resourse-book', RESBOOK_URL . 'admin/js/admin.js', array(), RESBOOK_VERSION );
?>
