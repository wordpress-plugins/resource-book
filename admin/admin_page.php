<div class=rb-main>
<?php


$subpage = isset($_GET['subpage']) ? sanitize_key($_GET['subpage']) : 'main';
$resource_id = isset($_GET['edit_res_id']) ? sanitize_key($_GET['edit_res_id']) : '';




    /**
    *   Список всех доступных разделов
    */
    global $wpdb;
    $allSection = $wpdb->get_results( "SELECT * FROM rs_book_section" );

    $sectionUsed = [];
    $rsData = array( 
        'rs_format' => '',     
        'rs_name'  => '',       
        'rs_description' => '',  
        'rs_author' => '',      
        'rs_cover' => '',       
        'rs_time_video' =>'',   
        'rs_file_doc' => '',     
        'rs_link_video' =>'' ,  
        'rs_link_site' => '' ,    
        'rs_date' => '',        
    );

    if(isset($_POST['rs-button'])){
        

        $rsFormat = $_POST['rs-format']; /* rs-book / rs-video / rs-link */
        
        $rsName     = $rsDescription    = $rsAuthor     = $rsCover        = '';
        $rsTime     = $rsHours          = $rsMinutes    = $rsSeconds    = 0;
        $rsFile     = $rsLink           = $rsSite       = '';
                   
        /*  rs-book/rs-video/rs-link    */
        $rsDescription  = $_POST[$rsFormat.'-description'];
        $rsAuthor       = $_POST[$rsFormat.'-author'];
        if(isset($_POST[$rsFormat.'-cover'])){
            $rsCover    = $_POST[$rsFormat.'-cover'];
        };
        
        switch ($rsFormat) {
            case 'rs-book':
            case 'rs-video':
                $rsName  = $_POST[$rsFormat.'-name'];                
                break;
        }
       
        switch ($rsFormat) {
            case 'rs-book':
                $rsFile  = $_FILES[$rsFormat.'-file'];

                $rsSuccess = 'КНИГА: ';
                break;
            case 'rs-video':
                $rsHours    = ($_POST[$rsFormat.'-hours']     == '' ? '0' : $_POST[$rsFormat.'-hours']);
                $rsMinutes  = ($_POST[$rsFormat.'-minutes']   == '' ? '0' : $_POST[$rsFormat.'-minutes']);
                $rsSeconds  = ($_POST[$rsFormat.'-seconds']   == '' ? '0' : $_POST[$rsFormat.'-seconds']);
                $rsLink     = $_POST[$rsFormat.'-link'];

                $rsSuccess = 'ВИДЕО: ';
                break;
            case 'rs-link':
                $rsSite  = $_POST[$rsFormat.'-site'];

                $rsSuccess = 'СТАТЬЯ: ';
                break;
        } 
       

        $mainDir = '/var/www/html/wp-content/uploads/rs_uploads/';
        $dirDocuments = $mainDir.'documents/';
        $dirCover = $mainDir.'cover/';
        $date = date('m-Y').'/';

        
        if (isset($_FILES[$rsFormat.'-file'])){
            $uploadDirDoc = $dirDocuments.$date;
            if (!file_exists($uploadDirDoc)) {
                mkdir($uploadDirDoc, 0777, true);
            }
            if($_FILES[$rsFormat.'-file']['name']){
                $uploadFile = $uploadDirDoc . str_replace(' ', '_', basename($_FILES[$rsFormat.'-file']['name']));
                if (move_uploaded_file($_FILES[$rsFormat.'-file']['tmp_name'], $uploadFile)) {
                    $rsFile = str_replace('/var/www/html/','',$uploadFile);
                } else {
                    if ($_POST['rs-button']=='Сохранить'){
                        $rsError['UploadsFile'] = 'Не получилось загрузить документ;';
                    }
                }
            } else {
                if (isset($_POST['rs-file-doc'])){
                    $rsFile = $_POST['rs-file-doc'];
                }
            }
        } 
        
        if (isset($_FILES[$rsFormat.'-cover'])){
            $uploadDirCover = $dirCover.$date;
            if (!file_exists($uploadDirCover)) {
                mkdir($uploadDirCover, 0777, true);
            }
            if ($_FILES[$rsFormat.'-cover']['name'] != ''){
                $uploadFile = $uploadDirCover .  str_replace(' ', '_', basename($_FILES[$rsFormat.'-cover']['name']));
                if (move_uploaded_file($_FILES[$rsFormat.'-cover']['tmp_name'], $uploadFile)) {
                    $rsCover = str_replace('/var/www/html/','',$uploadFile);
                } else {
                    if ($_POST['rs-button']=='Сохранить'){
                        $rsError['UploadsCover'] = 'Не получилось загрузить обложку;';
                    }
                }
            } else {
                if (isset($_POST['rs-cover'])){
                    $rsCover = $_POST['rs-cover'];
                }
            }
        }
        global $wpdb;
        
        $rsData = array( 
            'rs_format' => $rsFormat,     
            'rs_name'  => $rsName,       
            'rs_description' => $rsDescription,  
            'rs_author' => $rsAuthor,      
            'rs_cover' => $rsCover,       
            'rs_time_video' => $rsHours.':'.$rsMinutes.':'.$rsSeconds,   
            'rs_file_doc' => $rsFile,     
            'rs_link_video' => $rsLink ,  
            'rs_link_site' => $rsSite ,    
            'rs_date' => date('Y-m-d'),        
        );
        if ($_POST['rs-button']=='Сохранить'){
            $rsSuccess .= 'материал, успешно добавлен.';
            $request = $wpdb->insert('rs_book_resource', $rsData); 
    
            if ($request) {
                $lastid = $wpdb->insert_id;
    
                $index = 0;
                foreach ($allSection as $value){
                    if ($value->shortcode_section != ''){
                        if(isset($_POST['rs-section-'.$value->shortcode_section])){
                            $request = $wpdb->insert( 
                                'rs_published_resources', 
                                array( 
                                    'id_resource' => $lastid,      
                                    'id_section'  => $value->id,      
                                )
                            ); 
                            if (!$request){
                                $rsError['recordBD'] = 'Не получилось добавить раздел к ресурсу;';
                            }
                            $sectionUsed[$index++] = [
                                'id_resource'=> $lastid,
                                'id_section' =>$value->id
                            ];
                        }
                    }
                }
            } else {
                $rsError['recordBD'] = 'Не получилось записать ресурс;';
            }
        }
        if ($_POST['rs-button']=='Обновить'){
            $rsSuccess .= 'материал, успешно обновлен.';

            $wpdb->update( 'rs_book_resource', $rsData, array( 'id' => $resource_id ));
            $wpdb->delete( 'rs_published_resources', array( 'id_resource' => $resource_id ));

            $index = 0;
            foreach ($allSection as $value){
                if ($value->shortcode_section != ''){
                    if(isset($_POST['rs-section-'.$value->shortcode_section])){
                        $request = $wpdb->insert( 
                            'rs_published_resources', 
                            array( 
                                'id_resource' => $resource_id,      
                                'id_section'  => $value->id,      
                            )
                        ); 
                        if (!$request){
                            $rsError['recordBD'] = 'Не получилось добавить раздел к ресурсу;';
                        }
                        $sectionUsed[$index++] = [
                            'id_resource'=> $resource_id,
                            'id_section' =>$value->id
                        ];
                    }
                }
            }
        }
    }
?>


<?php






/**
*   Объявление меню страницы настроек  
*/
//esc_url(add_query_arg('subpage','main'))
//add_query_arg( array('subpage'=>'main','edit_res_id'=>''))
$url = add_query_arg( array('delet_res'=>false, 'edit_res_id'=>false));
$out = '
    <nav class="rb-nav-menu">
        <a class="rb-nav '.active_nav($subpage, 'main').'" href="'.esc_url(add_query_arg( 'subpage','main', $url)).'" >Таблица</a>
        <a class="rb-nav '.active_nav($subpage, 'add_new').'" href="'.esc_url(add_query_arg( 'subpage','add_new', $url)).'" >Создать Ресурс</a>
        <a class="rb-nav '.active_nav($subpage, 'setting').'" href="'.esc_url(add_query_arg( 'subpage','setting', $url)).'" >Настройки</a>
    </nav>';
echo $out;

function active_nav($subpage, $nav){
    return ($subpage == $nav ? 'active': '');
}       
?>

    <div class="rb-container">

    

<?php 
/**
*   подключение нужной страницы меню
*/
    switch ($subpage) {
        //страница доступных ресурсов
        case 'main':
            // здесь получают все возможные ресурсы
            
            require_once RESBOOK_PLUGIN_DIR.'admin/admin_main.php';
            break;
        //страница создания(или)обновления ресурса
        case 'add_new':
            if (isset($rsError)) {
                $spanError = '<span class="rs-cheсk-control rs-input-error"></span>';
                $messageError = 'Пожалуйста, сообщите разработчику о случившейся ошибке, укажите какие ошибки произошли и какое название ресурса было указанно вами.';
                
                echo '<table style="border-bottom:2px solid red;">';
                    if(isset($rsError['UploadsFile'])){
                        echo '<tr><td>'.$spanError.'</td><td>'.$rsError['UploadsFile'].'</td></tr>';
                    }
                    if(isset($rsError['UploadsCover'])){
                        echo '<tr><td>'.$spanError.'</td><td>'.$rsError['UploadsCover'].'</td></tr>';
                    }
                    if(isset($rsError['recordBD'])){
                        echo '<tr><td>'.$spanError.'</td><td>'.$rsError['recordBD'].'</td></tr>';
                    }
                    echo '<tr><td colspan=2>'.$messageError.'</td></tr>';
                echo '</table>';
            } else {
                if (isset($rsSuccess)){
                    $spanSuccess = '<span class="rs-cheсk-control rs-input-successfully"></span>';
                    echo '<table style="border-bottom:2px solid green;">';
                        echo '<tr><td>'.$spanSuccess.'</td><td>'.$rsSuccess.'</td></tr>';
                    echo '</table>';
                }
            }
            require_once RESBOOK_PLUGIN_DIR.'admin/admin_addnew.php';
            break;
        //страница настроек    
        case 'setting':
            require_once RESBOOK_PLUGIN_DIR.'admin/admin_setting.php';
            break;
    }
?>
    </div>
    <!-- /.rb-container -->
</div>
<!-- /.rb-main -->