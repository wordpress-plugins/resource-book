window.onload = function(){

    get_card_video();
}

var Cards = {
    Block: {},
    Path: {},
    videoBlock: '',
    videSRC: 'https://www.youtube.com/embed/',
}

function get_card_video(){
    Cards.Block = {};
    Cards.Path = {};
    let cards = document.querySelectorAll('.content-card.card-video');

    cards.forEach(card => {
        let id = card.id.replace('id-','');
        Cards.Block[id]  = document.getElementById('id-video-'+id);
        Cards.Path[id]   = document.getElementById('video-path-'+id).value;
    });

    for (card in Cards.Block){
        let element = Cards.Block[card];

        element.addEventListener('click', open_pop_up)
    };
}

function open_pop_up(){
    let id = this.id.replace('id-video-','')
    let path = Cards.Path[id];

    let iframe = document.createElement('iframe');
    iframe.width = '99%';
    iframe.height = '450px';
    iframe.setAttribute('allowfullscreen', '');
    iframe.src = Cards.videSRC+path;

    document.getElementById('pop-up-video').append(iframe);

    document.getElementById('pop-up-main').style.display='block';
    setTimeout(() => {
        document.getElementById('pop-up-main').style.opacity= 100;
    }, 500);
    document.getElementById('pop-up-button').addEventListener('click', ()=>{
        document.getElementById('pop-up-main').style.opacity= 0;
        setTimeout(() => {
            document.getElementById('pop-up-main').style.display='none';
            iframe.remove();
        }, 1000);
    })
}