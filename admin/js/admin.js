window.addEventListener("DOMContentLoaded", function(event) {

    get_item();
    Index = Resourse.ResFormat.value;
    listen_input();

    Resourse.ResFormat.addEventListener('change', open_table);

    document.getElementById('rs-button').addEventListener('click', send_form);
});


var Resourse = {
    ResFormat       : '',
    ResTable        : {},
    DescrLength     : 0,
    NameLength      : 0,
    AuthorLength    : 0,

};

var Index ='';

function get_item(){
    Resourse.ResFormat = document.getElementById('rs-format');

    let table = document.querySelectorAll('.rs-table')
    
    table.forEach(tab =>{
        let index = tab.id;
        Resourse.ResTable[index] = tab;
    });
};

function open_table(){
    /**отменить предыдущий листнер */
    Index = Resourse.ResFormat.value;

    for(key in Resourse.ResTable){
        Resourse.ResTable[key].classList.add('rs-table-hidden');
    }

    Resourse.ResTable[Index].classList.remove('rs-table-hidden');
    
    document.getElementById('rs-message').innerText = '';

    listen_input();
};

function listen_input(){
    console.log(Index)
    console.log('______________________default________________________')
    /*
    * input        rs-book-description                 rs-video-description            rs-link-description
    * span         rs-bookbook-symbol-description      rs-video-symbol-description     rs-link-symbol-description
    * blur         rs-book-check-description           rs-video-check-description      rs-link-check-description
    * 
    * input        rs-book-author                      rs-video-author                 rs-link-author
    * span         rs-book-symbol-author               rs-video-symbol-author          rs-link-symbol-author
    * blur         rs-book-check-author                rs-video-check-author           rs-link-check-author
    * 
    * input        rs-book-cover                       rs-video-cover                  rs-link-cover
    * change       rs-book-check-cover                 rs-video-check-cover            rs-link-check-cover
    * */
   console.log(Index+'-description')

   /*
    document.getElementById(Index+'-description').oninput=  (e)=>{
        console.log(Index +'//'+e.target.id)
        console.log(e.target.value.length)
        console.log(this);
    };
*/
    document.getElementById(Index+'-description').onblur = (e)=>{
        if(e.target.value.length <= 8){
            document.getElementById(Index+'-check-description').classList.add('rs-input-error');
        } else {
            document.getElementById(Index+'-check-description').classList.add('rs-input-successfully');
        }
    };
    document.getElementById(Index+'-description').onfocus = (e)=>{
        if (document.getElementById(Index+'-check-description').classList.contains('rs-input-error')){
            document.getElementById(Index+'-check-description').classList.remove('rs-input-error');
        }
        if (document.getElementById(Index+'-check-description').classList.contains('rs-input-successfully')){
            document.getElementById(Index+'-check-description').classList.remove('rs-input-successfully');
        }
    };

/*   console.log(Index+'-symbol-description')
   console.log(Index+'-check-description')
*/

document.getElementById(Index+'-author').onblur = (e)=>{
    if(e.target.value.length == 0){
        document.getElementById(Index+'-check-author').classList.add('rs-input-error');
    } else {
        document.getElementById(Index+'-check-author').classList.add('rs-input-successfully');
    }
};
document.getElementById(Index+'-author').onfocus = (e)=>{
    if (document.getElementById(Index+'-check-author').classList.contains('rs-input-error')){
        document.getElementById(Index+'-check-author').classList.remove('rs-input-error');
    }
    if (document.getElementById(Index+'-check-author').classList.contains('rs-input-successfully')){
        document.getElementById(Index+'-check-author').classList.remove('rs-input-successfully');
    }
};
/*
   console.log(Index+'-author ')
   console.log(Index+'-symbol-author')
   console.log(Index+'-check-author')
  */ 


document.getElementById(Index+'-cover').onchange = (e)=>{
    if(e.target.value.length == 0){
        document.getElementById(Index+'-check-cover').classList.add('rs-input-error');
    } else {
        document.getElementById(Index+'-check-cover').classList.add('rs-input-successfully');
    }
};
document.getElementById(Index+'-cover').onfocus = ()=>{
    if (document.getElementById(Index+'-check-cover').classList.contains('rs-input-error')){
        document.getElementById(Index+'-check-cover').classList.remove('rs-input-error');
    }
    if (document.getElementById(Index+'-check-cover').classList.contains('rs-input-successfully')){
        document.getElementById(Index+'-check-cover').classList.remove('rs-input-successfully');
    }
};
/*
   console.log(Index+'-cover')
   console.log(Index+'-check-cover')
*/

    if ((Index == ('rs-book'))||(Index == ('rs-video'))){
        console.log('__________________rs-book/rs-video____________________')

    /*
    * input        rs-book-name                        rs-video-name
    * span         rs-book-symbol-name                 rs-video-symbol-name
    * blur         rs-book-check-name                  rs-video-check-name
    */
        
        document.getElementById(Index+'-name').onblur = (e)=>{
            if(e.target.value.length <= 7){
                document.getElementById(Index+'-check-name').classList.add('rs-input-error');
            } else {
                document.getElementById(Index+'-check-name').classList.add('rs-input-successfully');
            }
        };
        document.getElementById(Index+'-name').onfocus = (e)=>{
            if (document.getElementById(Index+'-check-name').classList.contains('rs-input-error')){
                document.getElementById(Index+'-check-name').classList.remove('rs-input-error');
            }
            if (document.getElementById(Index+'-check-name').classList.contains('rs-input-successfully')){
                document.getElementById(Index+'-check-name').classList.remove('rs-input-successfully');
            }
        };
        /**
         * console.log(Index+'-name ')
        console.log(Index+'-symbol-name')
        console.log(Index+'-check-name')
         */

        if (Index == 'rs-book'){
            console.log('______________________rs-book________________________')
            /*
            *   input        rs-book-file
            * change       rs-book-check-file
            */ 
            
            document.getElementById(Index+'-file').onchange = (e)=>{
                if(e.target.value.length == 0){
                    document.getElementById(Index+'-check-file').classList.add('rs-input-error');
                } else {
                    document.getElementById(Index+'-check-file').classList.add('rs-input-successfully');
                }
            };
            document.getElementById(Index+'-file').onfocus = ()=>{
                if (document.getElementById(Index+'-check-file').classList.contains('rs-input-error')){
                    document.getElementById(Index+'-check-file').classList.remove('rs-input-error');
                }
                if (document.getElementById(Index+'-check-file').classList.contains('rs-input-successfully')){
                    document.getElementById(Index+'-check-file').classList.remove('rs-input-successfully');
                }
            };
            /*
           console.log(Index+'-file')
           console.log(Index+'-check-file')*/
        }
        if (Index == 'rs-video'){
            console.log('______________________rs-video________________________')

            /*
            * input rs-video-hours rs-video-minutes rs-video-seconds
            * change rs-video-check-minutes
            * 
            * input rs-video-link
            * span rs-video-check-link
             */
            console.log(Index+'-hours')
            Index+'hours'
            Index+'minutes'
            Index+'seconds'
            console.log(Index+'hours')
            console.log(document.getElementById(Index+'-hours'))
            document.getElementById(Index+'-hours').onblur = (e)=>{
                if ((e.target.value < 10)&&(e.target.value >=0)){
                    let minutes = document.getElementById(Index+'-minutes').value;
                    if ((minutes  < 60 )&&(minutes >=0)){
                        let seconds = document.getElementById(Index+'-seconds').value
                        if ((seconds  < 60 )&&(seconds >=0)){
                            document.getElementById(Index+'-check-minutes').classList.add('rs-input-successfully');
                        }else {
                            document.getElementById(Index+'-check-minutes').classList.add('rs-input-error');
                        }
                    } else {
                        document.getElementById(Index+'-check-minutes').classList.add('rs-input-error');
                    }
                } else {
                    document.getElementById(Index+'-check-minutes').classList.add('rs-input-error');
                }
            };
            document.getElementById(Index+'-minutes').onblur = (e)=>{
                if ((e.target.value  < 60 )&&(e.target.value >=0)){
                    let hour = document.getElementById(Index+'-hours').value;
                    
                    if ((hour  < 10 )&&(hour >= 0)){
                        let seconds = document.getElementById(Index+'-seconds').value
                        if ((seconds  < 60 )&&(seconds >=0)){
                            document.getElementById(Index+'-check-minutes').classList.add('rs-input-successfully');
                        }else {
                            document.getElementById(Index+'-check-minutes').classList.add('rs-input-error');
                        }
                    } else {
                        document.getElementById(Index+'-check-minutes').classList.add('rs-input-error');
                    }
                } else {
                    document.getElementById(Index+'-check-minutes').classList.add('rs-input-error');
                }
            };
            document.getElementById(Index+'-seconds').onblur = (e)=>{
                if ((e.target.value  < 60 )&&(e.target.value >=0)){
                    let hour = document.getElementById(Index+'-hours').value;
                    if ((hour  < 10 )&&(hour >=0)){
                        let minutes = document.getElementById(Index+'-minutes').value
                        if ((minutes  < 60 )&&(minutes >=0)){
                            document.getElementById(Index+'-check-minutes').classList.add('rs-input-successfully');
                        }else {
                            document.getElementById(Index+'-check-minutes').classList.add('rs-input-error');
                        }
                    } else {
                        document.getElementById(Index+'-check-minutes').classList.add('rs-input-error');
                    }
                } else {
                    document.getElementById(Index+'-check-minutes').classList.add('rs-input-error');
                }
            };
            document.getElementById(Index+'-hours').onfocus = (e)=>{
                if (document.getElementById(Index+'-check-minutes').classList.contains('rs-input-error')){
                    document.getElementById(Index+'-check-minutes').classList.remove('rs-input-error');
                }
                if (document.getElementById(Index+'-check-minutes').classList.contains('rs-input-successfully')){
                    document.getElementById(Index+'-check-minutes').classList.remove('rs-input-successfully');
                }
            }
            document.getElementById(Index+'-minutes').onfocus = (e)=>{
                if (document.getElementById(Index+'-check-minutes').classList.contains('rs-input-error')){
                    document.getElementById(Index+'-check-minutes').classList.remove('rs-input-error');
                }
                if (document.getElementById(Index+'-check-minutes').classList.contains('rs-input-successfully')){
                    document.getElementById(Index+'-check-minutes').classList.remove('rs-input-successfully');
                }
            }
            document.getElementById(Index+'-seconds').onfocus = (e)=>{
                if (document.getElementById(Index+'-check-minutes').classList.contains('rs-input-error')){
                    document.getElementById(Index+'-check-minutes').classList.remove('rs-input-error');
                }
                if (document.getElementById(Index+'-check-minutes').classList.contains('rs-input-successfully')){
                    document.getElementById(Index+'-check-minutes').classList.remove('rs-input-successfully');
                }
            }



            document.getElementById(Index+'-link').onblur = (e)=>{
                if(e.target.value.length == 0){
                    document.getElementById(Index+'-check-link').classList.add('rs-input-error');
                } else {
                    document.getElementById(Index+'-check-link').classList.add('rs-input-successfully');
                }
            };
            document.getElementById(Index+'-link').onfocus = (e)=>{
                if (document.getElementById(Index+'-check-link').classList.contains('rs-input-error')){
                    document.getElementById(Index+'-check-link').classList.remove('rs-input-error');
                }
                if (document.getElementById(Index+'-check-link').classList.contains('rs-input-successfully')){
                    document.getElementById(Index+'-check-link').classList.remove('rs-input-successfully');
                }
            };
            /*
            console.log(Index+'-link')
            console.log(Index+'-check-link')*/
        }
    }
    if (Index == 'rs-link'){
        console.log('______________________rs-link________________________')

    /*
    *   input rs-link-site
    *   span rs-link-check-site
    * */

        document.getElementById(Index+'-site').onblur = (e)=>{
            if(e.target.value.length == 0){
                document.getElementById(Index+'-check-site').classList.add('rs-input-error');
            } else {
                document.getElementById(Index+'-check-site').classList.add('rs-input-successfully');
            }
        };
        document.getElementById(Index+'-site').onfocus = (e)=>{
            if (document.getElementById(Index+'-check-site').classList.contains('rs-input-error')){
                document.getElementById(Index+'-check-site').classList.remove('rs-input-error');
            }
            if (document.getElementById(Index+'-check-site').classList.contains('rs-input-successfully')){
                document.getElementById(Index+'-check-site').classList.remove('rs-input-successfully');
            }
        };
        /*
        console.log(Index+'-site')
        console.log(Index+'-check-site')*/
   }

}


function send_form(e){
    let sendForm = true;
    if (!document.getElementById(Index+'-check-description').classList.contains('rs-input-successfully')){
        document.getElementById(Index+'-check-description').classList.add('rs-input-error');
        sendForm = false;
    }
    if (!document.getElementById(Index+'-check-author').classList.contains('rs-input-successfully')){
        document.getElementById(Index+'-check-author').classList.add('rs-input-error');
        sendForm = false;
    }
    if ((Index == ('rs-book'))||(Index == ('rs-video'))){
        if (!document.getElementById(Index+'-check-name').classList.contains('rs-input-successfully')){
            document.getElementById(Index+'-check-name').classList.add('rs-input-error');
            sendForm = false;
        }
        if (Index == 'rs-book'){
            if (!document.getElementById(Index+'-check-file').classList.contains('rs-input-successfully')){
                document.getElementById(Index+'-check-file').classList.add('rs-input-error');
                sendForm = false;
            }
        }
        if (Index == 'rs-video'){
            if (!document.getElementById(Index+'-check-minutes').classList.contains('rs-input-successfully')){
                document.getElementById(Index+'-check-minutes').classList.add('rs-input-error');
                sendForm = false;
            }
            if (!document.getElementById(Index+'-check-link').classList.contains('rs-input-successfully')){
                document.getElementById(Index+'-check-link').classList.add('rs-input-error');
                sendForm = false;
            }
        }
    }
    if (Index == 'rs-link'){
        if (!document.getElementById(Index+'-check-site').classList.contains('rs-input-successfully')){
            document.getElementById(Index+'-check-site').classList.add('rs-input-error');
            sendForm = false;
        }
    }
    
    if (!sendForm){
        e.preventDefault();
        document.getElementById('rs-message').innerText = 'Вы заполнили не все обязательные поля';
    }
    
}