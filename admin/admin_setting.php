<?php 
class TableSection {
    protected $section_count;
    protected $section_use;

    protected $section;

    protected $table_header;
    protected $table_body;

    function __construct($section=null,$id=null){
        if (isset($section)){
            $this->section_count = count($section);
            $this->section = $section;

            if (is_int($id)){
                $this->section_use = $id;
            }
        }
        
        $this->created_header();
        $this->created_section_row();

        $this->wrap_body();

    }
   
    protected function created_header(){
        $code = '<td width=330 rowspan=2>создать новый раздел</td>';
        $code .= '<td colspan=4>список всех разделов</td>';

        $code = '<tr>'.$code.'</tr>';
        $code .= ' <tr><td>Название</td><td>Шорткод</td><td colspan=2>Команды</td></tr>';

        $this->table_header = '<thead>'.$code.'</thead>';
    }
    protected function created_section_row(){
        $code = $this->created_add_section();
        if ($this->section_count > 0){
            foreach ($this->section as $key => $value) {
                $td  = '<td>'.$value->name_section.'</td>';
                $td .= '<td>['.$value->shortcode_section.']</td>';
                $td .= '<td><span class="rs-table-icon rs-icon-bin" title="Удалить элемент"></span></td>';
                $td .='<td><span class="rs-table-icon rs-icon-pencil" title="Изменить элемент"></span></td>';

                if ($key == 0){
                    $code = '<tr>'.$code.$td.'</tr>';
                } else {
                    $code .= '<tr>'.$td.'</tr>';
                }
            }
        } else {
            $td = '<td colspan=4>Еще не добавленно ни одного раздела, воспользуйтесь формой!</td>';
            $code = '<tr>'.$code.$td.'</tr>';
        }

        $this->table_body = '<tbody>'.$code.'</tbody>';
    }
    protected function created_add_section(){
        $code = '<td rowspan='.$this->section_count.'>';
        $form =     '<label>Название раздела: <input type="text" name="section-name"/></label>';
        $form .=    '<label>Шорткод раздела: <input type="text" name="section-shortcode"/></label>';
        $form .=    '<input type="submit" name="add-section" value="Добавить"/>';

        $form = '<form method="POST" action="'.esc_url(remove_query_arg('msg')).'">'.$form.'</form>';
        $code .= $form.'</td>';

        return $code;
    }

    protected function wrap_body(){
        $code = $this->table_header;
        $code .= $this->table_body;

        $code = '<table border="1" cellpadding="10" class="rs-list-table">'.$code.'</table>';

        echo $code;
    }
}

if (isset($_POST['add-section'])){
   switch ($_POST['add-section']) {
        case 'Добавить':
            $name = $_POST['section-name'];
            $shortcode = $_POST['section-shortcode'];

            echo 'Имя: '.$name.'<br>';
            echo 'Шорткод: '.$shortcode.'<br>';

            break;
        case 'Обновить':
            # code...
            break;
   }
} 


global $wpdb;
$section = $wpdb->get_results("SELECT * FROM rs_book_section");
new TableSection($section);

?>