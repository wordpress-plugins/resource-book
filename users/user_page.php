<?php 
    global $wpdb; 
    # все доступные разделы #
    $allSection = $wpdb->get_results( "SELECT * FROM rs_book_section" );

    $idSection = 0;
    $html_user_table = '';

    if ($atts["section"] != 'all'){
        foreach ($allSection as $value){
            if ($value->shortcode_section == $atts["section"]){
               $idSection = $value->id;
            } 
        }
        if ($idSection != 0) {
            // получаем ресурсы опубликованные в этом разделе;
            $request = $wpdb->get_results("
                SELECT rs_book_resource.id, rs_format, rs_name, rs_description, rs_author, rs_cover, rs_time_video, rs_file_doc, rs_link_video, rs_link_site, rs_date FROM rs_published_resources, rs_book_resource 
                WHERE rs_published_resources.id_section = {$idSection} 
                AND rs_published_resources.id_resource = rs_book_resource.id;");

            if (count($request) > 0){
                foreach ($request as $value) {
                    switch ($value->rs_format) {
                        case 'rs-book':
                            $html_user_table .= construct_users_book($value); 
                            break;
                        case 'rs-video':
                            $html_user_table .= construct_users_video($value);
                            break;
                        case 'rs-link':
                            $html_user_table .= construct_users_link($value);
                            break;
                    }
                }
            } else {
                $html_user_table = '<div>Пока ничего нет :( Но, мы обязательно, что нибудь добавим :) Заходите позже! Будем ждать :)</div>';
            }
        } else {
            $html_user_table = '<div>Что-то пошло не так... сообщите администратору, спасибо!</div>';
        }
    } else {
        $html_user_table = '<div>Упс, мы еще не готовы все показать.. Скажите администратору, что бы он поторопился:)</div>';
    }

    $html = construct_wrap_request($html_user_table);


    function construct_wrap_request($str){
        $html = '
            <div class="cards-block">
                <div class="pop-up-main" id="pop-up-main">
                    <div class="pop-up-container">
                        <span class="pop-up-button" id="pop-up-button">X</span>
                        <div class="pop-up-video" id="pop-up-video">
                        </div>
                    </div>
                </div>'.$str.'
                <script type="text/javascript" src="http://portal/wp-content/themes/PortalVtg33/assets/js/cards.js"></script>
            </div>
        ';
        return $html;
    }


    function construct_users_book ($value){
        $html = '
        <div class="content-card card-book" id="">
            <a href="'.get_site_url().'/'.$value->rs_file_doc.'" target="_blank" class="card-UI" id="" rel="noopener noreferrer">
                <div class="card-img" style="background-image: url('.create_cover_res($value->rs_format, $value->rs_cover).')">
                </div>
                <div class="card-vinetka"></div>
            </a>
            <div class="description-card">
                <span class="card-title">'.$value->rs_name.'</span>
                <span class="card-delineation">'.$value->rs_description.'</span>
                <span class="card-author">'.$value->rs_author.'</span>
                <span class="card-format">'.get_format_file($value->rs_file_doc).'</span>
                <span class="card-time">'.get_date_res($value->rs_date).'</span>
            </div>
        </div>
        ';
        return $html;
    }
    function construct_users_video ($value){
        $html = '
        <div class="content-card card-video" id="id-'.$value->id.'">
            <div class="card-UI" id="id-video-'.$value->id.'">
                <div class="card-img" style="background-image: url('.create_cover_res($value->rs_format, $value->rs_cover).')">
                </div>
                <div class="card-vinetka"></div>
                <input type="text" id="video-path-'.$value->id.'" value="'.$value->rs_link_video.'" hidden="">
            </div>
            <div class="description-card">
                <span class="card-title">'.$value->rs_name.'</span>
                <span class="card-delineation">'.$value->rs_description.'</span>
                <span class="card-author">'.$value->rs_author.'</span>
                <span class="card-duration">'.$value->rs_time_video.'</span>
                <span class="card-time">'.get_date_res($value->rs_date).'</span>
            </div>
        </div>
        ';
         
        return $html;
    }

    function construct_users_link ($value){
        $html = '
        <div class="content-card card-paper" id="">
            <a href="'.$value->rs_link_site.'" target="_blank" class="card-UI" id="" rel="noopener noreferrer">
                <div class="card-img" style="background-image: url('.create_cover_res($value->rs_format, $value->rs_cover).')">
                </div>
                <div class="card-vinetka"></div>
            </a>
            <div class="description-card">
                <span class="card-title">'.$value->rs_description.'</span>
                <span class="card-author">'.$value->rs_author.'</span>
                <span class="card-site">'.get_url_into_link($value->rs_link_site).'</span>
                <span class="card-time">'.get_date_res($value->rs_date).'</span>
            </div>
        </div>
        ';
        return $html;
    }

    function get_date_res($data){
        list ($year, $month, $day) = explode ('-', $data);
        $day .= ' ';
        switch ($month) {
            case '01':  $month = ' января, ';   break;
            case '02':  $month = ' февраля, ';  break;
            case '03':  $month = ' марта, ';    break;
            case '04':  $month = ' апреля, ';   break;
            case '05':  $month = ' мая, ';      break;
            case '06':  $month = ' июня, ';     break;
            case '07':  $month = ' июля, ';     break;
            case '08':  $month = ' августа, ';  break;
            case '09':  $month = ' сентября, '; break;
            case '10':  $month = ' октября, ';  break;
            case '11':  $month = ' ноября, ';   break;
            case '12':  $month = ' декабря, ';  break;
        }
        $year .= ' года';

        return $day.$month.$year;
    }
    function create_cover_res($format,$cover){
        $url = get_site_url().'/';
        if ($cover != ''){
            $url .= $cover;
        } else {
            $url = RESBOOK_URL.'users/img/';
            switch ($format) {
                case 'rs-book':
                    $url .= 'null_book.png';
                    break;
                case 'rs-video':
                    $url .= 'null_video.png';
                    break;
                case 'rs-link':
                    $url .= 'null_link.png';
                    break;
            }
        }
        return $url;
    }
    function get_format_file($str){
        list ($none, $format) = explode ('.', $str);
        return '.'.$format;
    }
    function get_url_into_link($str){
        $str = explode ('/', $str);
        return $str[2];
    }
?>

